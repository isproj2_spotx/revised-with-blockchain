# DISH 2018 Notes
## Theory basis
### Blockchain Trilemma
#### Implementation
- Decentralization
	- Provide a decentralized environment to provide better connections between therapist and client
- Security
	- Tokenization for privacy of transactions, privacy of medical records
- Scalability
#### Sources
 [The Blockchain Trilemma - Learn How Can We Solve It](https://www.coinreview.com/blockchain-trilemma/)
[How to Scale Ethereum: Sharding Explained – Prysmatic Labs – Medium](https://medium.com/prysmatic-labs/how-to-scale-ethereum-sharding-explained-ba2e283b7fce)
#### Choices
- Read/write - public/private, open/closed
- Storage - on chain/ decentralized
- Consensus model - (block speed, cost of transactions)
- Programmability (smart contracts, configurable transactions)
- Access - API/Nodes

### Tweaks and performance 
- Sharding - Breaks down transactions into smaller ‘chunks’ of data. Instead of every single node in the network processing whole transactions, nodes are separated into groups, and specific chunks of data are processed by these groups of nodes. Later in the process, these chunks of data are re-assimilated to be stored permanently on the blockchain.
- Side chains - On the Ethereum Virtual Network (EVM), there exists the possibility of creating a side network upon which a project can process its specific transactions, then record only the beginning and ending outcomes to the Ethereum network. This reduces the strain on the EVM, but places trust in the management of the side chain, thus placing trust in a third party, reducing decentralization.

### Ideas	
- Multiple blockchains in conjunction - multiple decentralized blockchains
- Incentivize home program compliance 
- Delegate call , scalable side chain - loom network
- 3box.js
- database -> hash to the side chain -> deliver to the etherealmnetwork
- [Holochain | Think Outside the Blocks - scalable distributed computing](https://holochain.org/)
- [MultiChain | Open source blockchain platform](https://www.multichain.com/)

### Business Plan
[Pitch Deck Template - Google Slides](https://docs.google.com/presentation/d/1J9Esz6zKLrrq6fNfr32EXZ4T9WoNBqr3cS5-DjTmYW8/edit#slide=id.g474d146407_0_9)
- **Problem** 
	- #### Continuance of therapy care
		- Platform to help clients and therapist offer even at home
		- Privacy of data
		- Security of transactions
		- Integrity of platform
- Business Model Canvas 
	- #### What do you do?
		- Provide a platform for therapists and their clients to bridge care between the home where all things happen and the therapy center 
	- #### How do you do it?
		- Make an application to check if they're doing their home programs
		- Use blockchain protocol to provide security and decentralization to preserve therapeutic relationship
		- Log records of progress and visualize for better communication between the client and therapist
	- #### What do you need?
		- Ethereum Network 
		- Cloud platform
	- #### Who will help you?
		- Therapist organizations
		- Therapy Centers
	- #### How do you interact?
		- Social Media
		- Website chat support
	- #### How do you reach them?
		- Partner with centers
		- Partner with organizations
		- Digital marketing
	- #### Who do you help?
		- Therapists
		- Their clients
	- What will cost?
		- Subscription Model
			- Phase 1: partner with centers
			- Phase 2: open for freelance and public

### Architecture Design

[image:9B180635-011C-42CD-ABF8-03449A2D6AA4-6993-000027FB353CC3F3/Screen Shot 2018-11-24 at 12.45.44 PM.png]
[image:CF2C6BD4-4C27-48B2-B0EC-0FD9762C407B-6993-00002A61B5248255/Screen Shot 2018-11-24 at 1.30.12 PM.png]


### Contract Specifications
- Access control - ownable -> ownership in contracts
- Deploy multiple contracts?
- bitbit
- Form shards between client and therapist, therapist in multiple shards?
- Users
	- Therapist - multiple client
	- Client - one therapist
- Constructor
	- therapistAdd
	- clientAdd
	- taskAdd
	- taskAssetProofURI
	- timestamp?
	- 
- Flow
	- Sign validation of transaction
	
### Flow
- Therapist makes a task for the client to the client
- Client Perform the tasks
- Transaction


### Possible truffle boxes to use
[Truffle Suite | Boxes | rapid-box](https://truffleframework.com/boxes/rapid-box)
https://truffleframework.com/boxes

### Linnia JS
[GitHub - ConsenSys/linnia-js: Linnia JavaScript API](https://github.com/ConsenSys/linnia-js)
https://github.com/ConsenSys/Linnia-Smart-Contracts

attestation
encryption
linnia consensus -> reward the user 

To do - encrypt decrypt with linna

use multiple contracts:
- User properties
- Transaction attestation

i need
- a token as assets


### Front end notes
- Create task
- Perform task
- Vue or react?

### Backend notes
* Communicate with the blockchain - Linnia JS

"The goal of therapy is to improve the quality of life, and I think a blockchain solution is a therapeutic approach for a better future."


[Understanding Blockchain Privacy — Anonymity, Encryption and Decentralization](https://hackernoon.com/understanding-blockchain-privacy-anonymity-encryption-and-decentralization-30ed4fddb808)
[Tone Vays Says Bitcoin Bull Run Will Leave Crypto Rivals in the Dust, Plus Ripple and XRP, Ethereum, Tron, EOS, Bitcoin SV: Crypto News Alert | The Daily Hodl](https://dailyhodl.com/2018/12/30/tone-vays-says-bitcoin-bull-run-will-leave-crypto-rivals-in-the-dust-plus-ripple-and-xrp-ethereum-tron-eos-bitcoin-sv-crypto-news-alert/?fbclid=IwAR3cWVBrZfUAYnisPM-5_9rhYqVBd_HpRHNi7gJvvqdh7hzw4oodpL8TNv8) -- Attestation




